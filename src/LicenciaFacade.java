public class LicenciaFacade {

    private Licencia licenciaA1;

    public LicenciaFacade(String dni){
        licenciaA1=new Licencia();
        licenciaA1.setDni(dni);
        licenciaA1.setCategoria("A");
        licenciaA1.setTipo("I");
    }

    public String verDetalle(){
        return licenciaA1.verDetalle();
    }
}
