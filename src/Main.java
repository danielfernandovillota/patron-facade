public class Main {

    public static void main(String[] args){

        //Codigo sin patron Facade

        Licencia licencia=new Licencia();
        System.out.println("LICENCIA 1");
        licencia.setDni("1085345273");
        licencia.setCategoria("A");
        licencia.setTipo("I");
        System.out.println(licencia.verDetalle());

        //Codigo con patron Facade
        System.out.println("LICENCIA 2");
        LicenciaFacade facade=new LicenciaFacade("1085345273");
        System.out.println(facade.verDetalle());
    }
}
