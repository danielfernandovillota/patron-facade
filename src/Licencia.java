public class Licencia {

    private String dni;
    private String categoria;
    private String tipo;

    public Licencia(){

    }

    public String getDni(){
        return dni;
    }

    public String getCategoria(){
        return categoria;
    }

    public String getTipo(){
        return tipo;
    }

    public void setDni(String dni){
        this.dni=dni;
    }

    public void setCategoria(String categoria){
        this.categoria=categoria;
    }

    public void setTipo(String tipo){
        this.tipo=tipo;
    }

    public String verDetalle(){
        return "DNI: "+dni+" CATEGORIA: "+categoria+" TIPO: "+tipo;
    }
}
